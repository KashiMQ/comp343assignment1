package kashi.comp343.test;

import kashi.comp343.keys.KeyScheduler;
import kashi.comp343.util.KeyChain;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

import static org.junit.Assert.*;

/**
 * Tests the key scheduling algorithm to ensure that it's moving things around
 * correctly.
 */
public class KeySchedulerTest {

    @Test
    public void rotateZeros() throws Exception {
        byte[] k = {0b0,0b0};
        KeyScheduler key = new KeyScheduler(k);
        KeyChain keys = key.forwards();

        for (int i = 0; i < 8; i++)
            assertEquals(keys.nextKey(), 0b0);
    }

    @Test
    public void rotateOnes() throws Exception {
        byte[] k = {0b01, 0b01};
        KeyScheduler key = new KeyScheduler(k);
        KeyChain keys = key.forwards();

        assertEquals(0b00000001, keys.nextKey());
        assertEquals(0b00000001, keys.nextKey());

        // First derived permutation
        assertEquals(0b00101000, keys.nextKey());
        assertEquals(0b01100001, keys.nextKey());
    }

    @Test
    public void rotateRealNumbers() throws Exception {
        byte[] k = new byte[] { (byte)57, (byte)-4 };
        KeyScheduler key = new KeyScheduler(k);
        KeyChain keys = key.forwards();

        // First two bytes
        assertEquals((byte)57, keys.nextKey());
        assertEquals((byte)-4, keys.nextKey());

        // Mutations
        assertEquals((byte)-64, keys.nextKey());
        assertEquals((byte)-103, keys.nextKey());
        assertEquals((byte)-44, keys.nextKey());
        assertEquals((byte)-107, keys.nextKey());
        assertEquals((byte)54, keys.nextKey());
        assertEquals((byte)3, keys.nextKey());

    }

    @Test
    public void rotationCoverage() throws Exception {
        byte[] k = new byte[] { (byte)57, (byte)-4 };
        KeyScheduler key = new KeyScheduler(k);
        KeyChain keyChain = key.forwards();

        ArrayList<Byte> keys = new ArrayList<Byte>();

        for (int i = 0; i < 8; i++) {
            keys.add(keyChain.nextKey());
        }

        for (int i = 0; i < 1000; i++) {
            byte refKey = keys.get(i % 8);
            byte nextKey = keyChain.nextKey();
            assertEquals(refKey, nextKey);
        }

    }
}