package kashi.comp343.util;

import com.sun.javaws.exceptions.InvalidArgumentException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Created by Kashi on 1/04/2016.
 */
public class BitMechanicTests {

    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @Test
    public void splitByte() throws Exception {
        // Are we splitting correctly
        byte a = (byte)0b00000000;
        assertEquals((byte)0b00000000, BitMechanic.splitByte(a)[0]);
        assertEquals((byte)0b00000000, BitMechanic.splitByte(a)[1]);

        byte b = (byte)0b11111111;
        assertEquals((byte)0b00001111, BitMechanic.splitByte(b)[0]);
        assertEquals((byte)0b00001111, BitMechanic.splitByte(b)[1]);

        byte c = (byte)0b11110000;
        assertEquals((byte)0b00000000, BitMechanic.splitByte(c)[0]);
        assertEquals((byte)0b00001111, BitMechanic.splitByte(c)[1]);

        byte d = (byte)0b00001111;
        assertEquals((byte)0b00001111, BitMechanic.splitByte(d)[0]);
        assertEquals((byte)0b00000000, BitMechanic.splitByte(d)[1]);

        byte e = (byte)0b10101010;
        assertEquals((byte)0b00001010, BitMechanic.splitByte(e)[0]);
        assertEquals((byte)0b00001010, BitMechanic.splitByte(e)[1]);

        byte f = (byte)0b01010101;
        assertEquals((byte)0b00000101, BitMechanic.splitByte(f)[0]);
        assertEquals((byte)0b00000101, BitMechanic.splitByte(f)[1]);

        byte g = (byte)0b11000011;
        assertEquals((byte)0b00000011, BitMechanic.splitByte(g)[0]);
        assertEquals((byte)0b00001100, BitMechanic.splitByte(g)[1]);

        byte h = (byte)0b00111100;
        assertEquals((byte)0b00001100, BitMechanic.splitByte(h)[0]);
        assertEquals((byte)0b00000011, BitMechanic.splitByte(h)[1]);
    }

    @Test
    public void combineHalfBytes() throws Exception {
        // Check to see that 4-bit numbers are being combined correctly.
        final byte[][] xorTests = {
                //  Number a          Number b          a XOR b
                new byte[] {
                        (byte)0b00000000, (byte)0b00000000, (byte)0b00000000
                },
                new byte[] {
                        (byte)0b00000101, (byte)0b00001010, (byte)0b10100101
                },
                new byte[] {
                        (byte)0b00001111, (byte)0b00001010, (byte)0b10101111
                },
                new byte[] {
                        (byte)0b00001010, (byte)0b00000101, (byte)0b01011010
                }
        };

        for (int i = 0; i < xorTests.length; i++)
            assertEquals(
                    xorTests[i][2],
                    BitMechanic.combineHalfBytes(
                        new byte[] {xorTests[i][0], xorTests[i][1]}
                    )
            );

        // Also check that an error is thrown for illegal numbers
        exception.expect(IllegalArgumentException.class);
        BitMechanic.combineHalfBytes(
                new byte[] {(byte)0b10000000, 0b0}
        );
    }

    @Test
    public void xor() throws Exception {
        // XOR operations testing
        final byte[][] xorTests = {
            //  Number a          Number b          a XOR b
            new byte[] {
                (byte)0b00000000, (byte)0b00000000, (byte)0b00000000
            },
            new byte[] {
                (byte)0b01010101, (byte)0b10101010, (byte)0b11111111
            },
            new byte[] {
                (byte)0b11111111, (byte)0b10101010, (byte)0b01010101
            },
            new byte[] {
                (byte)0b11111111, (byte)0b01010101, (byte)0b10101010
            },
            new byte[] {
                (byte)0b11111111, (byte)0b11111111, (byte)0b00000000
            }
        };

        // Run the tests
        for (int i = 0; i < xorTests.length; i++)
            assertEquals(
                xorTests[i][2],
                BitMechanic.xor(xorTests[i][0], xorTests[i][1])
            );

    }

    @Test
    public void rotateLeft() throws Exception {
        // Check that rotations are taking place in a non-destructive
        // manner.
        final byte[][] rotateLeftTests = {
            new byte[] {
                0b00000001, 0b00000010, 0b00000100, 0b00001000,
                0b00010000, 0b00100000, 0b01000000, (byte)0b10000000
            },
            new byte[] {
                (byte)0b00101011, (byte)0b01010110, (byte)0b10101100,
                (byte)0b01011001, (byte)0b10110010, (byte)0b01100101,
                (byte)0b11001010, (byte)0b10010101
            },
            new byte[] {
                (byte)0b11111100, (byte)0b11111001, (byte)0b11110011,
                (byte)0b11100111, (byte)0b11001111, (byte)0b10011111,
                (byte)0b00111111, (byte)0b01111110
            }
        };

        for (int i = 0; i < rotateLeftTests.length; i++)
            for (int j = 0; j < rotateLeftTests[i].length; j++)
                assertEquals(
                    (byte)rotateLeftTests[i][j],
                    (byte)BitMechanic.rotateLeft(rotateLeftTests[i][0], j)
                );

    }
}