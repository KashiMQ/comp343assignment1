package kashi.comp343.cipher;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Kashi on 1/04/2016.
 */
public class FeistelCipherTest {

    @Test
    public void roundTrip() throws Exception {
        byte[] plaintext = new byte[] { (byte)84, (byte)104 };
        byte[] key = new byte[] { (byte)57, (byte)-4 };

        FeistelCipher cipher = new FeistelCipher();
        cipher.loadKey(key);

        byte[] cipherText = cipher.encrypt(plaintext);
        byte[] decryptedText = cipher.decrypt(cipherText);

        cipher.deleteKey();

        assertEquals(plaintext, decryptedText);
    }

}