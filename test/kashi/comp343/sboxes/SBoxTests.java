package kashi.comp343.sboxes;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Tests our S-box, specifically for coverage of low-order bytes (that is, 4-bit
 * binary numbers masquerading as bytes) and raising exceptions when fed with
 * higher-order numbers.
 * @author Kashi Samaraweera <kashi.samaraweera@students.mq.edu.au>
 * @version 0.1.0
 */
public class SBoxTests {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void fourBitPermutations() throws Exception {
        // Do we have unique permutations for all 4-bit numbers?
        HashSet<Byte> unique = new HashSet<Byte>();
        for (int i = 0; i < 16; i++)
            unique.add(SBox.fourBitPermute((byte)i));

        assertEquals(16, unique.size());
    }

    @Test
    public void lowOrderBytes() {
        // Is our S-box returning 4-bit numbers?
        for (int i = 0; i < 16; i++) {
            assert (SBox.fourBitPermute((byte) i) < 16);
            assert (SBox.fourBitPermute((byte) i) >= 0);
        }
    }

    @Test
    public void highOrderBytes() {
        // S-box should throw if we try to operate on > 4-bit numbers
        exception.expect(IndexOutOfBoundsException.class);
        SBox.fourBitPermute((byte)0b00010000);
    }

    @Test
    public void negativeBytes() {
        // S-box should throw if we try to operate on numbers that feature a
        // leading 1 (negatives)
        exception.expect(IndexOutOfBoundsException.class);
        SBox.fourBitPermute((byte)0b10000000);
    }

}