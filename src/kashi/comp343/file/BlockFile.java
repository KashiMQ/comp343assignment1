package kashi.comp343.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.List;
/**
 * Created by Kashi on 24/03/2016.
 */
public class BlockFile {

    RandomAccessFile file;
    String filePath;

    /**
     * Construct a BlockFile pointing and supply it with a path to a real file
     * on the device.
     * @param filePath  The file that will be either read from or written to.
     */
    public BlockFile(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Organises the file into blocks, supplying it as a LinkedList of byte
     * pairs.
     * @return  Returns the file in its entireity, broken up into byte pairs
     *          ready for encryption.
     */
    public byte[] getBlocks() throws IOException {
        file = new RandomAccessFile(filePath, "r");
        byte[] fileBytes = new byte[(int)file.length()];
        try {
            file.read(fileBytes);
        } catch (IOException e) {
            throw new IOException(String.format("There was an error " +
                    "reading the file '%s'.", filePath));
        }
        return fileBytes;
    }

    /**
     * Sets the content of the file by determining the blocks that make up its
     * contents.
     * @param fileBlocks    A collection of 2-byte blocks that should be
     *                      written to the disk on save.
     * @return
     */
    public void setBlocks(byte[] fileBlocks) throws IOException {

        file = new RandomAccessFile(filePath, "rw");

        try {
            file.write(fileBlocks);
        } catch (IOException e) {
            throw new IOException(String.format("There was an error " +
                    "writing to the file '%s'.", filePath));
        }

    }
}
