package kashi.comp343.keys;

import kashi.comp343.util.BitMechanic;
import kashi.comp343.util.KeyChain;

import java.util.Collections;
import java.util.LinkedList;

/**
 * A simple key scheduler that takes a 16-bit key K, producing key variations
 * based on this key for an infinite number of rounds.
 */
public class KeyScheduler {

    final int K_MINUS_1_ROTATION = 3;
    final int K_MINUS_2_ROTATION = 5;

    byte[] key;
    int dispatched = 0;

    LinkedList<Byte> keyPermutations = new LinkedList<Byte>();
    LinkedList<KeyChain> issuedKeyChains = new LinkedList<KeyChain>();

    /**
     * Constructs a key scheduler with a supplied byte array used as the key.
     * @param key   The intended key.
     */
    public KeyScheduler(byte[] key) {
        if (key.length != 2) throw new IllegalArgumentException("Invalid " +
                "key. This cipher accepts 16-bit keys only.");
        this.key = key;
        generateKeypermutations();
    }

    /**
     * Constructs a key scheduler with a supplied short (16-bit value) as the
     * key.
     * @param key   The intended key.
     */
    public KeyScheduler(short key) {
        this.key = BitMechanic.splitShort(key);
        generateKeypermutations();
    }

    /**
     * Creates each of the keys K0..K7, storing them in the local property
     * for easy access.
     */
    protected void generateKeypermutations() {
        keyPermutations.add(key[0]);
        keyPermutations.add(key[1]);

        for (int i = 2; i < 8; i++) {
            byte kMin1 = keyPermutations.get(i - 1);
            byte kMin2 = keyPermutations.get(i - 2);

            byte kMin1R = BitMechanic.rotateLeft(kMin1, K_MINUS_1_ROTATION);
            byte kMin2R = BitMechanic.rotateLeft(kMin2, K_MINUS_2_ROTATION);

            byte kPerm = BitMechanic.xor(kMin1R, kMin2R);

            keyPermutations.add(kPerm);
        }
    }

    /**
     * Overwrites the stored key with zeros and chases after all the issued
     * KeyChain objects with a kill order.
     */
    public void destroyKey() {
        for (int i = 0; i < key.length; i++) {
            key[i] = (byte)0b0;
        }
        for (KeyChain k : issuedKeyChains)
            k.delete();

        issuedKeyChains.clear();
        keyPermutations.clear();

    }

    /**
     * Creates a revolving key chain with the keys 0..7 organised in natural
     * their natural order. This method creates a copy of the current key
     * permutations before assigning it to the KeyChain.
     * @return  Returns a KeyChain object with a built in iterator to manage
     *          key scheduling. Keys are arranged in natural order.
     */
    public KeyChain forwards() {
        LinkedList<Byte> fwdKeyPermutations =
                new LinkedList<>(keyPermutations);

        KeyChain fwdChain = new KeyChain(fwdKeyPermutations);
        issuedKeyChains.add(fwdChain);

        return fwdChain;
    }

    /**
     * Creates a revolving key chain with the keys 0..7 organised in reverse
     * order, (7..0). This method creates a copy of the current key
     * permutations before assigning it to the KeyChain.
     * @return  Returns a KeyChain object with a built in iterator to manage
     *          key scheduling. Keys are arranged in reverse order.
     */
    public KeyChain reverse() {
        LinkedList<Byte> revKeyPermutations =
                new LinkedList<>(keyPermutations);

        Collections.reverse(revKeyPermutations);

        KeyChain reverseChain = new KeyChain(revKeyPermutations);
        issuedKeyChains.add(reverseChain);

        return reverseChain;
    }

}
