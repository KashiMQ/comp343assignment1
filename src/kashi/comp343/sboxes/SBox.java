package kashi.comp343.sboxes;

import kashi.comp343.util.BitMechanic;

import static kashi.comp343.util.BitMechanic.splitByte;

/**
 * An implementation of an S-Box with published transformations conducted on
 * 4-bit binary strings.
 * @author Kashi Samaraweera <kashi.samaraweera@students.mq.edu.au>
 * @version 0.1.0
 */
public class SBox {

    // Our S-box permutations stored in an easy-to-access manner.
    final public static byte[][] mappings = {new byte[] {
            0b0000, 0b0001, 0b0010, 0b0011,
            0b0100, 0b0101, 0b0110, 0b0111,
            0b1000, 0b1001, 0b1010, 0b1011,
            0b1100, 0b1101, 0b1110, 0b1111
    }, new byte[] {
            0b0000, 0b0001, 0b1011, 0b1101,
            0b1001, 0b1110, 0b0110, 0b0111,
            0b1100, 0b0101, 0b1000, 0b0011,
            0b1111, 0b0010, 0b0100, 0b1010
    }};

    /**
     * Operates on a 4-bit string cast as a byte, transforming the input value
     * to the output value of the S-box as per the mapping common to this class.
     * This method will throw an error if the input byte exceeds 2^4.
     * @param a
     * @return
     */
    public static byte fourBitPermute(byte a) {
        for (int i = 0; i < mappings[0].length; i++)
            if (a == mappings[0][i])
                return mappings[1][i];

        throw new ArrayIndexOutOfBoundsException(String.format("This S-box " +
                "is designed to take 4-bit values only. The value supplied " +
                "exceeds (%d).", a));
    }

    /**
     * Performs a permutation on a full 8-bit byte, calling upon the
     * fourBitPermute method to do so.
     * This method will handle the splitting and re-combining of a full byte so
     * that it is ready for the next step in the Feisel network.
     * @param a A full 8-bit byte to process through the S-box.
     * @return  An 8-bit byte that has been processed through the S-box.
     */
    public static byte permutation(byte a) {
        byte[] halfBytes = BitMechanic.splitByte(a);

        halfBytes[0] = fourBitPermute(halfBytes[0]);
        halfBytes[1] = fourBitPermute(halfBytes[1]);

        return BitMechanic.combineHalfBytes(halfBytes);
    }

}
