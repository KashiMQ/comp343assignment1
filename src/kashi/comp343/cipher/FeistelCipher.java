package kashi.comp343.cipher;

import kashi.comp343.keys.KeyScheduler;
import kashi.comp343.sboxes.SBox;
import kashi.comp343.util.BitMechanic;
import kashi.comp343.util.KeyChain;
import les.comp343.Cipher;

import java.util.Collections;
import java.util.LinkedList;

/**
 * FeistelCipher class
 *
 * A concrete implementation of the Cipher interface, providing Feistel-type
 * encryption when given plaintext and a suitable key.
 */
public class FeistelCipher implements Cipher {

    KeyScheduler key;

    final public static int ROUNDS = 8;
    final public static int P_ROTATE = 2;

    /**
     * Load a key into the Cipher, perform any key setup, etc.
     *
     * @param key@return true on success
     */
    @Override
    public boolean loadKey(byte[] key) {
        try {
            this.key = new KeyScheduler(key);
        } catch (IllegalArgumentException e) {
            return false;
        }

        return true;
    }

    /**
     * Delete the key from the Cipher
     *
     * @return true on success
     */
    @Override
    public boolean deleteKey() {
        if (key == null) return false;

        key.destroyKey();
        return true;
    }

    /**
     * Encrypt a block of plaintext
     *
     * @param block
     * @return the block of ciphertext
     */
    @Override
    public byte[] encrypt(byte[] block) {
        KeyChain keysForward = key.forwards();
        return processBytes(block, keysForward);
    }

    /**
     * Decrypt a block of ciphertext
     *
     * @param block
     * @return the block of plaintext
     */
    @Override
    public byte[] decrypt(byte[] block) {
        KeyChain keysReverse = key.reverse();
        byte[] reverseBlock = new byte[block.length];
        for (int i = 0; i < block.length; i++)
            reverseBlock[i] = block[block.length - i - 1];

        return processBytes(reverseBlock, keysReverse);
    }

    /**
     * Implements the cipher mechanism. This method will pad any blocks that
     * are of odd length.
     * @param blocks    An array of blocks.
     * @param keys  A KeyChain instance containing the permutations of the
     *              encrpytion key.
     * @return  Returns an array of blocks that have been through the Fiestel
     *          network mechanism.
     */
    public byte[] processBytes(byte[] blocks, KeyChain keys) {
        if (key == null) throw new NullPointerException("The cipher key has " +
                "not been initialised.");

        LinkedList<Byte> inputStream = evenByteList(blocks);
        LinkedList<Byte> outputStream = new LinkedList<>();

        byte[] blockPair;
        while (inputStream.size() > 0) {
            blockPair = processBlock(new byte[]{
                inputStream.remove(),
                inputStream.remove()
            }, keys);
            outputStream.add(blockPair[0]);
            outputStream.add(blockPair[1]);
        }
        byte[] outputBytes = new byte[outputStream.size()];

        int i = 0;
        while (outputStream.size() > 0) {
            outputBytes[i] = outputStream.remove();
            i++;
        }

        return outputBytes;
    }

    /**
     * Takes a 2-byte block of plaintext, processing it through several rounds
     * of the Feistel network.
     * @param block A 16-bit block, technically 2 8-bit bytes.
     * @param keys  A KeyChain instance containing the permutations of the
     *              encryption key.
     * @return  Returns 16-bits (as two 8-bit bytes) that have been processed
     *          through several iterations of the Feistel network.
     */
    byte[] processBlock(byte[] block, KeyChain keys) {
        for (int i = 0; i < ROUNDS; i++) {
            byte roundkey = keys.nextKey();
            block = feistelStep(block[0], block[1], roundkey);
        }
        return block;
    }

    /**
     * A single iteration of the Feistel network data path.
     * @param l The leftmost 8-bits of what was originally a 16-bit block.
     * @param r The rightmost 8-bits of what was originally a 16-bit block.
     * @param keyBlock  The key permutation that pertains to this step.
     * @return  Returns a pair of 8-bit bytes (constituting our 16-bit block)
     *          that has been through a round of the Feistel network.
     */
    byte[] feistelStep(byte l, byte r, byte keyBlock) {
        // XOR our right half and Key iteration
        byte rKeyiXor = BitMechanic.xor(r, keyBlock);

        // Process our byte through the S-box
        byte sBoxedRKeyiX = SBox.permutation(rKeyiXor);

        // Apply our P bit rotation
        byte permSBRKeyiX = BitMechanic.rotateLeft(sBoxedRKeyiX, P_ROTATE);

        // Swap our halves, XORing the left half with the number computed using
        // our key, right half and S-box.
        return new byte[] {
            r,
            BitMechanic.xor(l, permSBRKeyiX)
        };
    }

    /**
     * Takes an array of blocks and adds padding to ensure there are an even
     * number of elements (if necessary).
     * @param blocks    An array of bytes.
     * @return  The same array of bytes padded with an extra zero-byte
     *          if the initial length was not even.
     */
    public LinkedList<Byte> evenByteList(byte[] blocks) {
        LinkedList<Byte> byteList = new LinkedList<Byte>();
        for (int i = 0; i < blocks.length; i++)
            byteList.add(blocks[i]);

        if (blocks.length % 2 == 1) byteList.add((byte)0);

        return byteList;
    }

}
