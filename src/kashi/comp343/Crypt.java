package kashi.comp343;

import kashi.comp343.cipher.FeistelCipher;
import kashi.comp343.file.BlockFile;
import kashi.comp343.util.BitMechanic;

import java.io.IOException;

import static javafx.application.Platform.exit;

public class Crypt {

    /**
     * Entry method, accepts space-separated command line arguments, collected
     * according to their position. These arguments are then fed into the crypt
     * and decrypt functions to resolve plaintext or ciphertext dependent on
     * the supplied key and Encrypt/Decrypt flag. See Readme.md for details.
     * @param args  The command line arguments supplied to the program
     *              executable for encryption or decryption. See readme.md for
     *              details.
     */
    public static void main(String[] args) {
        String pFilePath = args[0];
        String cFilePath = args[1];
        String key = args[2];
        String directionString = args[3];
        boolean encryption = true;

        try {
            encryption = isEncrypting(directionString);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            exit();
        }

        BlockFile plain = new BlockFile(pFilePath);
        BlockFile crypto = new BlockFile(cFilePath);

        Short keyHex = Short.parseShort(key);

        FeistelCipher cipher = new FeistelCipher();
        cipher.loadKey(BitMechanic.splitShort(keyHex));

        if (encryption) {
            System.out.println(
                String.format(
                    "Encrypting %s using the supplied key...",
                    pFilePath
                ));

            try {
                crypto.setBlocks(cipher.encrypt(plain.getBlocks()));
            } catch (IOException e) {
                System.out.println("File error.");
                e.printStackTrace();
            }

            System.out.println(String.format("All done. The ciphertext is " +
                    "now saved in %s.", cFilePath));
            return;
        }

        System.out.println(
            String.format(
                "Decrypting '%s' using the supplied key...",
                cFilePath
            ));

        try {
            crypto.setBlocks(cipher.encrypt(plain.getBlocks()));
        } catch (IOException e) {
            System.out.println("File error.");
            e.printStackTrace();
        }

        System.out.println(String.format("All done. The plaintext is " +
            "now saved in %s.", pFilePath));

    }

    /**
     * Takes the encryption/decryption argument from the command line and
     * determines whether we're trying to encrypt or decrypt based on the
     * presence of a capital E or capital D.
     * @param directionString   A string indicating either encryption or
     *                          decryption ("E" or "D" respectively).
     * @return boolean  Returns TRUE if the direction flag indicates encryption
     *                  or FALSE if it indicates decryption.
     * @throws IllegalArgumentException Thrown if the supplied string does not
     *                                  follow the expected syntax.
     */
    static boolean isEncrypting(String directionString) {
        if  (directionString.compareTo("E") == 0) return true;
        if  (directionString.compareTo("D") == 0) return false;
        throw new IllegalArgumentException(String.format("Encryption must " +
                "be indicated with a capital E, or decryption with a " +
                "capital D. The argument supplied `%s` is not accepted.",
                directionString));
    }
}
