package kashi.comp343.util;

import java.util.LinkedList;

/**
 * A utility class to help pull keys in succession. This helper utility must
 * be loaded with keys when initalised, and will be destroyed when a request
 * to delete key data is made to the parent KeyScheduler.
 */
public class KeyChain {

    LinkedList<Byte> keys;
    int keyIndex = 0;

    /**
     * Constructor for the keychain, which must be supplied with a set of keys.
     * @param keys
     */
    public KeyChain(LinkedList<Byte> keys) {
        this.keys = keys;
    }

    /**
     * Revolves the keychain and resolves the next key.
     * @return  Gets the next key in the chain, automatically looping through.
     */
    public byte nextKey() {
        return keys.get(keyIndex++ % 8);
    }

    /**
     * Deletes keys stored in the KeyChain.
     */
    public void delete() {
        keys = null;
    }
}
