package kashi.comp343.util;
import java.util.Arrays;
/**
 * A utility class that performs common (low-level) cryptographic operations on
 * bit sequences, passed around as byte arrays.
 * @author Kashi Samaraweera <kashi.samaraweera@students.mq.edu.au>
 * @version 0.1.0
 */
public class BitMechanic {

    /**
     * Takes a two-byte number (short) and resolves an array of (two) bytes.
     * @param s The 16-bit number to be separated into two bytes.
     * @return  Returns an array of two bytes,
     */
    public static byte[] splitShort(short s) {
        return new byte[] {(byte)(s & 0xff), (byte)((s >> 8) & 0xff)};
    }

    /**
     * Splits an 8-bit byte into two 4-bit bytes. This method will return an
     * array of two bytes containing 4-bit numbers (cast as byte) in bytes
     * low order and high order respectively.
     * @param block A Byte that should be split into its low order and high
     *              order components.
     * @return  Returns an array of 2 bytes based on the supplied byte.
     */
    public static byte[] splitByte(byte block) {
        return new byte[] {
            (byte)(block & 0b00001111),
            (byte)((block >> 4) & 0b00001111)
        };
    }

    /**
     * Combines two 4-bit bytes into a single whole byte, with the first
     * element occupying bits 0..3 and the second element bits 4..7.
     * @param blocks    An array of two bytes, each with values less than 2^4.
     * @return  Returns an 8-bit byte.
     * @throws IllegalArgumentException Thrown if either of the supplied bytes
     *                                  exceed the allowed bitspace.
     */
    public static byte combineHalfBytes(byte[] blocks)
            throws IllegalArgumentException {

        if (isFourBit(blocks[0]) == false || isFourBit(blocks[1]) == false)
            throw new IllegalArgumentException("This method will only work " +
                    "on 4-bit numbers (otherwise there will be data loss!");

        return (byte)(blocks[0] | (byte)(blocks[1] << 4));
    }

    /**
     * Checks to see if a supplied byte is 4-bits or greater.
     * @return  Returns TRUE if the number can be represented in four bits or
     *          FALSE if not.
     */
    public static boolean isFourBit(byte a) {
        return (a & 0b00001111) == a;
    }

    /**
     * Takes an array of an even number of bytes, returning an array of half
     * the length with the leftmost bytes.
     * @param block A block of an even length of bytes from which the
     *              resulting block should take half.
     * @return  An array of bytes containing the first n/2 bytes of the supplied
     *          block.
     */
    public static byte[] leftBlock(byte[] block) {
        return Arrays.copyOfRange(block, 0, block.length/2 - 1);
    }

    /**
     * Takes an array of an even number of bytes, returning an array of half
     * the length with the rightmost bytes.
     * @param block A block of an even length of bytes from which the
     *              resulting block should take half.
     * @return  An array of bytes containing the last n/2 bytes of the supplied
     *          block.
     */
    public static byte[] rightBlock (byte[] block) {
        return Arrays.copyOfRange(block, block.length/2, block.length - 1);
    }

    /**
     * XOR function two single byte instances.
     * @param a A single byte to be XOR'ed.
     * @param b A single byte to be XOR'ed.
     * @return The byte resulting from the XOR operation.
     */
    public static byte xor(byte a, byte b) {
        int aInt = (int)a;
        int bInt = (int)b;
        int xor = aInt ^ bInt;

        return (byte)xor;
    }

    /**
     * XOR function for an array of bytes of the same size.
     * @param a An array of bytes of length n.
     * @param b An array of bytes of length n.
     * @return  The array of bytes resulting from the XOR operation between
     *          corresponding bits.
     * @throws IllegalArgumentException Thrown if there is a length mismatch
     *                                  between the supplied byte arrays.
     */
    public static byte[] xor(byte[] a, byte[] b)
            throws IllegalArgumentException {

        if (a.length != b.length)
            throw new IllegalArgumentException("The bytes supplied must have " +
                    "an equal length.");

        byte[] xor = new byte[a.length];
        for (int i = 0; i < a.length; i++)
            xor[i] = xor(a[i], b[i]);

        return xor;
    }

    /**
     * Rotates a given byte by the given number of positions
     * @param a A byte to be rotated.
     * @param iPos  The number of bits to rotate the byte by.
     * @return  Returns a byte that has been rotated by iPos bits.
     */
    public static byte rotateLeft(byte a, int iPos) {
        for (int i = 0; i < iPos; i++)
            a = shiftLeft(a);
        return a;
    }

    /**
     * Rotates a byte left a single time.
     * @param a An 8-bit byte.
     * @return  Returns an 8-bit byte rotated circularly to the left.
     */
    static byte shiftLeft(byte a) {
        byte shifted = (byte)(a << 1);
        if (a < 0)
            shifted = (byte)(shifted | 0b1);

        return shifted;
    }
}
