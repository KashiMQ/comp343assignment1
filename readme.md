# Crypt
## A simple cypher implementation for encryption and decryption of text files
### COMP343 S1 2016 | Kashi Samaraweera (Student ID 40969819)

# **Please note that this solutin isn't working for a round-trip encryption/decryption!!**

#### Introduction
This is a simple implementation of a Fiestel-type cipher that operates on any file specified. It is done entirely within the JVM and thus should be avoided for any actual cryptographic purpose.

#### Implementation
The encryption mechanism is a reversible, block cipher that is loosely based on a Fiestel network. The mechanism applies 8 rounds for encryption and decryption, breaking an input into 16-bit blocks.

#### Build
This project is supplied with build configuration information for IDEA IntelliJ, although you should be able to build it using a native java build command.
**The build output is available at the following path:**
```
./out/artefacts/crypt_jar/crypt.jar
```

#### Usage
As a command line application, arguments are supplied to manipulate the program's input. The order of the arguments is supremely important, and appears as such:

```
java crypt plaintext-file ciphertext-file encryption-key E|D
```
##### Encryption
For encryption, the last arugment should be the character `E` (uppercase). For this operation any plaintext file can be used for the input, and the specified filename (or file path) will be used to store the ciphertext output. If a file already exists at the `ciphertext-file` location it will be overwritten.
Importantly, the key used for encryption is specified in the second-last argument. This should be a hexadecimal number between `0` and `ffff`, and prefixed with `0x` as per convention.
###### Example usage
```
java crypt plaintext.txt ciphertext.txt 0x10f0 E
```

##### Decryption
For decryption, a ciphertext file and plaintext file location should be specified. As with encryption, an encryption key must be supplied with the conventional `0x` prefix, representing a hexidecimal number (between 0-ffff).

The final arugment should be marked as character `D` (uppercase) to indicate decryption rather than incryption (failure to do so may overwrite the ciphertext file.)
###### Example usage
```
java crypt plaintext.txt ciphertext.txt 0x10f0 D
```

#### Tests
The project is shipped with unit tests, implementing the JUnit4 test suite. Configuration files for IntelliJ are included for the inclusion of JUnit4, however a native java compilation (or some other IDE) would require this library to be manually specified.

### License
**Creative Commons Zero v1.0 Universal**. See LICENSE.txt for details.
